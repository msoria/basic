<?php
use kartik\widgets\FileInput;
use kartik\helpers\Html; // or yii\helpers\Html
use kartik\widgets\ActiveForm; 
?>

<?php $form = ActiveForm::begin(['options' => ['enctype' => 'multipart/form-data']]) ?>

<?= $form->field($model, 'file')->fileInput() ?>

<button>Submit</button>

<?php ActiveForm::end() ?>

<?php echo $isUploaded ?>



