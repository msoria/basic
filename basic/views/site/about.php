<?php

/* @var $this yii\web\View */

use yii\helpers\Html;
use yii\helpers\Url;
use kartik\widgets\FileInput;


$this->title = 'About';
$this->params['breadcrumbs'][] = $this->title;
echo "<br><br><br><br><br>HELLOOOO";
?>
<div class="site-about">
    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        This is the About page. You may modify the following file to customize its content:
		<?php		 

		//multiple
		echo FileInput::widget([
    'name' => 'attachment_51',

    'options'=>[
        'multiple'=>false,
        'accept' => 'image/*' ],
   		'pluginOptions' => [
	    	'previewFileType' => 'image',
	    	'uploadUrl' => Url::to(['/site/file-upload']),
	    	'allowedFileExtensions' => ['jpg','gif','png']
  /*       'maxFileCount' => 10
        'showUpload' => false,
        'browseLabel' => '',
        'removeLabel' => '',
      'mainClass' => 'input-group-lg',
        'browseClass' => 'btn btn-primary btn-block',
        'browseIcon' => '<i class="glyphicon glyphicon-camera"></i> ',
        'browseLabel' =>  'Select Photo'*/
   			 ]
	]);
?>
    </p>

    <code><?= __FILE__ ?></code>
</div>
