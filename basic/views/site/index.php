<?php
use yii\jui\DatePicker;
/* @var $this yii\web\View */

$this->title = 'MangaMixs';
?>
<div class="site-index">

    <div class="jumbotron">
        <h1>Congratulations!</h1>

        <p class="lead">You have successfully created your Yii-powered application.</p>

        <p><a class="btn btn-lg btn-success" href="http://www.yiiframework.com">Get started with Yii</a></p>
    </div>

    <div class="body-content">

        <div class="row">
            <div class="col-lg-4">
<!--start-->    <h2>TESTING</h2>

                <?php use app\models\Country; 
                    use yii\helpers\Html;
use yii\web\CountryController;

                    $country = Country::findOne('US');
                    if($country)
                         print_r($country);
                    else{
                        $country1 = new Country();
                        $country1->code = "US";
                        $country1->name = "United States";
                        $country1->population = 322976000;
                        echo $country1->save();
                        echo "country added";
                    }
                ?>
                

<!--end-->
                <p><a class="btn btn-default" href="http://basic.dev/web/index.php/">Yii HOME &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <?= $value = Yii::$app->formatter->asDate("05-10-2017");

                echo DatePicker::widget([
                    'name'  => 'from_date',
                    'value'  => $value,
                    'language' => 'ja',
                    //'dateFormat' => 'yyyy-MM-dd',
                ]);

                ?>
                <br>

                <p><a class="btn btn-default" href="http://basic.dev/web/index.php/">HOME &raquo;</a></p>
            </div>
            <div class="col-lg-4">
                <h2>Heading</h2>

                <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et
                    dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip
                    ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu
                    fugiat nulla pariatur.</p>

                <p><a class="btn btn-default" href="http://www.yiiframework.com/extensions/">Yii Extensions &raquo;</a></p>
            </div>
        </div>

    </div>
</div>
